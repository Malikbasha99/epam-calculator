package EPAMProject.EPAMCalculator;

import java.util.Scanner;

public class Calculator {
	int num1,num2,choice;
	public void getElements(){
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter two elements :");
		num1=scan.nextInt();
		num2=scan.nextInt();
		System.out.println("Enter operation number :");		
		choice=scan.nextInt();
		scan.close();
	}
	public void Add(){
		System.out.println("Addition of "+num1+" + "+num2+" is "+(num1+num2));
	}
	public void Divide(){
		System.out.println("Division of "+num1+" / "+num2+" is "+(num1/num2));
	}
	public void Multiply(){
		System.out.println("Multiplication of "+num1+" * "+num2+" is "+(num1*num2));
	}
	public void Subtract(){
		System.out.println("Subtraction of "+num1+" - "+num2+" is "+(num1-num2));
	}
	public void Display(){
		System.out.println("1 Addition \n2 Division \n3 Multiplication \n4 Subtraction \n\n"); 
	}
	public void Operation(){
		
		switch(choice){
			case 1:
				Add();
			case 2:
				Divide();
			case 3:
				Multiply();
			case 4:
				Subtract();
		}
	}
	

	public static void main(String[] args) {
		Calculator cal = new Calculator();
		cal.Display();
		cal.getElements();
		cal.Operation();
	}

}
